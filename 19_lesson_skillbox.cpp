﻿#include <iostream>

class Animal
{
public:
    Animal()
    {}

    virtual void Voice()
    {
        std::cout << "(?_?)\n";
    }
};

class Dog: public Animal
{
public:
    Dog()
    {}
    
    void Voice() override
    {
        std::cout << "Woof!\n";
    }
};

class Cat : public Animal
{
public:
    Cat()
    {}

    void Voice() override
    {
        std::cout << "Meow!\n";
    }
};

class Duck : public Animal
{
public:
    Duck()
    {}

    void Voice() override
    {
        std::cout << "Quack!\n";
    }
};

class Cow : public Animal
{
public:
    Cow()
    {}

    void Voice() override
    {
        std::cout << "Moo!\n";
    }
};

class Fox : public Animal
{
public:
    Fox()
    {}

    void Voice() override
    {
        std::cout << "What does the fox say?\n";
    }
};

int main()
{

    Animal* a1 = new Dog();
    Animal* a2 = new Cat();
    Animal* a3 = new Duck();
    Animal* a4 = new Cow();
    Animal* a5 = new Fox();

    Animal* Zoo[5];
    Zoo[0] = a1;
    Zoo[1] = a2;
    Zoo[2] = a3;
    Zoo[3] = a4;
    Zoo[4] = a5;

    for (auto element : Zoo)
    {
        element->Voice();
    }
}
